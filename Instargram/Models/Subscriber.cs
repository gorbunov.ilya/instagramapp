﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Instargram.Models
{
    public class Subscriber : Entity
    {
        public User User { get; set; }

        public string UserFromId { get; set; }

        public string UserToId { get; set; }
    }
}
