﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Instargram.Models
{
    public class Comment : Entity

    {
    [Required]
    public string Text { get; set; }
    [Required]
    public string AuthorName { get; set; }
    public Publication Publication { get; set; }
    public int PublicationId { get; set; }
    public User User { get; set; }
    public string UserId { get; set; }
    }
}
