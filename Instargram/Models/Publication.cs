﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Instargram.Models
{
    public class Publication : Entity
    {
        public string ImagePath { get; set; }

        public string Desc { get; set; }

        public int LikeCount { get; set; }

        public User User { get; set; }
        public string UserId { get; set; }

        public List<Comment> Comments { get; set; }
        public List<Like> Likes { get; set; }
    }
}
