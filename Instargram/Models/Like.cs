﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Instargram.Models
{
    public class Like : Entity
    {
        public User User { get; set; }
        public string UserId { get; set; }

        public Publication Publication { get; set; }
        public int PublicationId { get; set; }

    }
}
