﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Instargram.Helper;
using Instargram.Models;
using Instargram.Repositories;
using Instargram.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Instargram.Controllers
{
    public class PublicationController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<User> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;

        public PublicationController(
            UserManager<User> userManager,IHostingEnvironment hostingEnvironment, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IActionResult Create()
        {
            return View(new PublicationViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> CreatePublication(PublicationViewModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                Publication publication = new Publication
                {
                    Desc = model.Description,
                    LikeCount = 0,
                    UserId = user.Id,
                    ImagePath = await ImageHelper.SaveAvatarAndGetFullPath(user.Id, model.Image, 
                        0, _hostingEnvironment, Guid.NewGuid())
            };
                await unitOfWork.Publications.CreateAsync(publication);
                user.PublicationCount++;
                await _userManager.UpdateAsync(user);
                await unitOfWork.CompleteAsync();
                
                return RedirectToAction("Index", "Home");
            }
        }

        public async Task<IActionResult> Publications(string id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var publications = await unitOfWork.Publications.GetAllAsync();
                var publ = publications.Where(c => c.UserId == id);
                return View(publ);
            }
            
        }

        public async Task<IActionResult> Details(int publicationId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var publication = await unitOfWork.Publications.GetByIdAsync(publicationId);
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                var comments = await unitOfWork.Comments.GetAllAsync();
                var sortComments = comments.Where(c => c.PublicationId == publicationId);

                Comment comment = new Comment()
                {
                    Publication = publication,
                    UserId = user.Id,
                    User = user,
                    PublicationId = publication.Id,

                };
                ViewBag.Comments = sortComments;
                return View(comment);
            }
        }

        public async Task<IActionResult> Like(int publicationId, string userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {   
                var likes = await unitOfWork.Likes.GetAllAsync();
                if(likes.Count == 0)
                {
                    Like like = new Like()
                    {
                        PublicationId = publicationId,
                        UserId = userId
                    };
                    await unitOfWork.Likes.CreateAsync(like);
                }
                var publication = await unitOfWork.Publications.GetByIdAsync(publicationId);
                if (publication.LikeCount != 0 && likes.Exists(c => c.UserId == userId &&
                                                                    c.PublicationId == publicationId))
                {
                    publication.LikeCount--;
                    var allLikes = await unitOfWork.Likes.GetAllAsync();
                    var like = likes.FirstOrDefault(c =>
                        c.UserId == userId && c.PublicationId == publicationId);
                    unitOfWork.Likes.RemoveAsync(like);
                }
                else
                {
                    publication.LikeCount++;
                }
                await unitOfWork.CompleteAsync();
                return RedirectToAction("Details", "Publication", new {publicationId});
            }
                
        }
    }
}