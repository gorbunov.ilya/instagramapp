﻿using System.Threading.Tasks;
using Instargram.Models;
using Instargram.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Instargram.Controllers
{
    public class ReviewController : Controller
    {
        private IUnitOfWorkFactory UnitOfWorkFactory { get; set; }
        public ReviewController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            UnitOfWorkFactory = unitOfWorkFactory;
        }

        
        public async Task<IActionResult> CreateReview(Comment comment)
        {
            using (var unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                await unitOfWork.Comments.CreateAsync(comment);
                await unitOfWork.CompleteAsync();

                return RedirectToAction("Details", "Publication", new{ comment.PublicationId });
            }
        }
    }
}