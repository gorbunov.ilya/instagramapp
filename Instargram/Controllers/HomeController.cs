﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Instargram.Models;
using Instargram.Repositories;
using Microsoft.AspNetCore.Identity;

namespace Instargram.Controllers
{
    public class HomeController : Controller
    {
        private IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<User> _userManager;

        public HomeController(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager )
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
        }

        

        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
                {
                    var publications = await unitOfWork.Publications.GetAllAsync();
                    var subscribers = await unitOfWork.Subscribers.GetAllAsync();
                    var user = await _userManager.FindByNameAsync(User.Identity.Name);
                    var subscribersPublications = new List<Publication>();
                    if (publications.Count > 0 && subscribers.Count > 0)
                    {
                        var listSubscribers = subscribers.Where(c => c.UserToId == user.Id).ToList();
                        for (int i = 0; i < publications.Count; i++)
                        {
                            for (int j = 0; j < listSubscribers.Count; j++)
                            {
                                if (publications[i].UserId == listSubscribers[j].UserFromId)
                                {
                                    subscribersPublications.Add(publications[i]);
                                }
                            }

                        }
                    }

                    subscribersPublications.Reverse();
                    return View(subscribersPublications);
                }
            }

            return View();

        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
