﻿using System.Linq;
using System.Threading.Tasks;
using Instargram.Models;
using Instargram.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Instargram.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public UserController(UserManager<User> userManager, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _userManager = userManager;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<IActionResult> Index(string name)
        {
            var user = await _userManager.FindByNameAsync(name);
            if (user != null)
            {
                return View(user);
            }

            return NotFound();
        }

        public IActionResult Search(string res, string key)
        {
            IQueryable<User> users;
            switch (key)
            {
                case "Поиск по логину":
                     users = _userManager.Users.Where(c => c.Email.Contains(res));
                    break;
                case "Почте":
                    users = _userManager.Users.Where(c => c.Email == res);
                    break;
                case "Имени":
                    users = _userManager.Users.Where(c => c.Name.Contains(res));
                    break;
                case "Информации о себе":
                    users = _userManager.Users.Where(c => c.AboutMe == res);
                    break;
                default:
                    users = _userManager.Users.Where(c => c.Email == res);
                    break;
            }

            return View(users);
        }

        public async Task<IActionResult> Subscribe(string userId)
        {
            using (var unitOfWork =_unitOfWorkFactory.MakeUnitOfWork())
            {
                var authorizeUser = await _userManager.FindByNameAsync(User.Identity.Name);
                authorizeUser.SubscriptionsCount++;
                var subscribeUser = await _userManager.FindByIdAsync(userId);
                subscribeUser.SubscribersCount++;
                await _userManager.UpdateAsync(authorizeUser);
                await _userManager.UpdateAsync(subscribeUser);
                Subscriber subscriber = new Subscriber(){UserFromId = userId, UserToId = authorizeUser.Id };
                await unitOfWork.Subscribers.CreateAsync(subscriber);
                await unitOfWork.CompleteAsync();
                return RedirectToAction("Index", "Home");
            }

        }
    }
}