﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Instargram.Data;

namespace Instargram.Repositories.Subscriber
{
    public class SubscriberRepository : Repository<Models.Subscriber>, ISubscribersRepository
    {
        public SubscriberRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

    public interface ISubscribersRepository : IRepository<Models.Subscriber>
    {
    }
}
