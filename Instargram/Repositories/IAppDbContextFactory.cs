﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Instargram.Data;
using Microsoft.EntityFrameworkCore;

namespace Instargram.Repositories
{
    public interface IAppDbContextFactory
    {
        ApplicationDbContext CreateContext();
    }

    public class AppDbContextFactory : IAppDbContextFactory
    {
        private readonly DbContextOptions _options;

        public AppDbContextFactory(
            DbContextOptions options)
        {
            _options = options;
        }

        public ApplicationDbContext CreateContext()
        {
            return new ApplicationDbContext(_options);
        }
    }
}
