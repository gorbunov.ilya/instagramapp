﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Instargram.Data;

namespace Instargram.Repositories.Publication
{
    public class PublicationRepository : Repository<Models.Publication>, IPublicationRepository
    {
        public PublicationRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
