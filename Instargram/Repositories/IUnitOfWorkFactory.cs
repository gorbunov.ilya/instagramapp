﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Instargram.Repositories
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork MakeUnitOfWork();
    }

    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IAppDbContextFactory _applicationDbContextFactory;

        public UnitOfWorkFactory(IAppDbContextFactory applicationDbContextFactory)
        {
            _applicationDbContextFactory = applicationDbContextFactory;
        }

        public IUnitOfWork MakeUnitOfWork()
        {
            return new UnitOfWork(_applicationDbContextFactory.CreateContext());
        }
    }
}
