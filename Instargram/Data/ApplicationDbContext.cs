﻿using System;
using System.Collections.Generic;
using System.Text;
using Instargram.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Instargram.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public DbSet<Publication> Publications { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Subscriber> Subscribers { get; set; }
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Models.Publication>()
                .HasOne(c => c.User)
                .WithMany(p => p.Publications)
                .HasForeignKey(k => k.UserId);

            modelBuilder.Entity<Models.Comment>()
                .HasOne(c => c.Publication)
                .WithMany(p => p.Comments)
                .HasForeignKey(k => k.PublicationId);

            modelBuilder.Entity<Models.Comment>()
                .HasOne(c => c.User)
                .WithMany(p => p.Comments)
                .HasForeignKey(k => k.UserId);
            modelBuilder.Entity<Subscriber>()
                .HasOne(c => c.User)
                .WithMany(s => s.Subscribers)
                .HasForeignKey(k => k.UserFromId);

            modelBuilder.Entity<Like>()
                .HasKey(bc => new { bc.PublicationId, bc.UserId });
            modelBuilder.Entity<Like>()
                .HasOne(bc => bc.Publication)
                .WithMany(b => b.Likes)
                .HasForeignKey(bc => bc.PublicationId);
            modelBuilder.Entity<Like>()
                .HasOne(bc => bc.User)
                .WithMany(c => c.Likes)
                .HasForeignKey(bc => bc.UserId);
        }
    }
}
