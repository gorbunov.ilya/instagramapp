﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Instargram.Data.Migrations
{
    public partial class AddUserFromIdInSubscribersTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subscribers_AspNetUsers_UserId",
                table: "Subscribers");

            migrationBuilder.DropIndex(
                name: "IX_Subscribers_UserId",
                table: "Subscribers");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Subscribers",
                newName: "UserToId");

            migrationBuilder.AlterColumn<string>(
                name: "UserToId",
                table: "Subscribers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserFromId",
                table: "Subscribers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscribers_UserFromId",
                table: "Subscribers",
                column: "UserFromId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subscribers_AspNetUsers_UserFromId",
                table: "Subscribers",
                column: "UserFromId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subscribers_AspNetUsers_UserFromId",
                table: "Subscribers");

            migrationBuilder.DropIndex(
                name: "IX_Subscribers_UserFromId",
                table: "Subscribers");

            migrationBuilder.DropColumn(
                name: "UserFromId",
                table: "Subscribers");

            migrationBuilder.RenameColumn(
                name: "UserToId",
                table: "Subscribers",
                newName: "UserId");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Subscribers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscribers_UserId",
                table: "Subscribers",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subscribers_AspNetUsers_UserId",
                table: "Subscribers",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
